# Stacks of Paper

The skeuomorphic interface paradigm for the rough year range of 1900 to 1960. 

- [ ] typewriter monospace fonts
- [ ] inbox/outbox trays

Cute details
- [ ] crooked rubber stamps (application denied!)
- [ ] coffee stains
- [ ] hole punches
- [ ] staples
- [ ] dog ears

Some quality of life things would be implemented, such as being able to click on section titles in an index to jump around pages.